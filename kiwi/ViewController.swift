//
//  ViewController.swift
//  kiwi
//
//  Created by Dmitriy Avvakumov on 23/01/2019.
//  Copyright © 2019 DimaAvvakumov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
    
    let bag = DisposeBag()
    
    var titles:[Int:String] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
    }

    func configureTableView() {
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
    }
    
    func title(at index: Int) -> String {
        return titles[index] ?? "Cell \(index)"
    }

}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let index = indexPath.row
        
        cell.textLabel?.text = title(at: index)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        pushSecondController(at: indexPath.row)
    }
}

extension ViewController {
    
    func pushSecondController(at index: Int) {
        let input = SecondModule.Input(text: title(at: index))
        let(controller, output) = try! SecondModule.module(input)
        
        // bind output
        output.data.subscribe(onNext: { [weak self] (text) in
            self?.titles[index] = text
            self?.tableView.reloadData()
        }).disposed(by: bag)
        
        navigationController?.pushViewController(controller, animated: true)
    }
    
}
