//
//  SecondViewController.swift
//  kiwi
//
//  Created by Dmitriy Avvakumov on 23/01/2019.
//  Copyright © 2019 DimaAvvakumov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

public class SecondModule {
    
    // Input
    public struct Input {
        let text: String
    }
    
    // Output
    public struct Output {
        let data: Observable<String>
    }
    
    public class func module(_ input: Input) throws -> (UIViewController, Output) {
        // create controller
        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SecondViewController") as! SecondViewController
        
        // create output
        let output = controller.configureModule(input)
        
        return (controller, output)
    }
}

public class SecondViewController: UIViewController {
    
    @IBOutlet var textFiled: UITextField!
    
    // Private
    private var inputText: String = ""
    private let outputData = PublishSubject<String>()
    
    // Life cicle
    public override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigation()
        textFiled.text = inputText
    }
    
    private func configureNavigation() {
        let item = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneAction))
        navigationItem.rightBarButtonItem = item
    }
    
    // Module configurator
    internal func configureModule(_ input:SecondModule.Input) -> SecondModule.Output {
        inputText = input.text
        
        return SecondModule.Output(
            data: outputData.asObservable()
        )
    }

    @objc private func doneAction(_ sender: UIBarButtonItem) {
        if let text = textFiled.text, text.count > 0 {
            outputData.onNext(text)
        }
        navigationController?.popViewController(animated: true)
    }

}
